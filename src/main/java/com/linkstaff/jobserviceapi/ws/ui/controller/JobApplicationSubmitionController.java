package com.linkstaff.jobserviceapi.ws.ui.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.linkstaff.jobserviceapi.ws.service.JobApplicationSubmitionService;
import com.linkstaff.jobserviceapi.ws.shared.dto.JobApplicationSubmitionDto;
import com.linkstaff.jobserviceapi.ws.ui.model.request.JobApplicationSubmitionRequestModel;
import com.linkstaff.jobserviceapi.ws.ui.model.response.JobApplicationSubmitionResponseModel;
import com.linkstaff.jobserviceapi.ws.utils.mapper.JobApplicationSubmitionMapper;

@RestController
@RequestMapping(value = "/")
public class JobApplicationSubmitionController {
	
	@Autowired
	JobApplicationSubmitionService jobApplicationSubmitionService;
	
	@Autowired
	JobApplicationSubmitionMapper jobApplicationSubmitionMapper;
	
	@GetMapping(value = "jobs/{jobId}/applicants")
	public List<JobApplicationSubmitionResponseModel> getApplicantByJobId(
			@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "limit", defaultValue = "25") int limit,
			@RequestParam(value = "sortby", defaultValue = "") String sortBy,
			@RequestParam(value = "searchby", defaultValue = "") String searchBy, 
			@PathVariable Long jobId) {
		
		
		List<JobApplicationSubmitionResponseModel> jobs = new ArrayList<JobApplicationSubmitionResponseModel>();
		List<JobApplicationSubmitionDto> jobsDto = jobApplicationSubmitionService.getAllJobApplicantByJobId(page, limit, jobId);
		for(JobApplicationSubmitionDto jobApplicationSubmitionDto: jobsDto){
			jobs.add(jobApplicationSubmitionMapper.mapFromJobApplicationSubmitionDtoToJobApplicationSubmitionResponseModel(jobApplicationSubmitionDto));
		} 
		
		return jobs;
		
	}
	
	@GetMapping(value = "profiles/{userId}/applied-jobs")
	public List<JobApplicationSubmitionResponseModel> getAppliedJobsByUserId(
			@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "limit", defaultValue = "25") int limit,
			@RequestParam(value = "sortby", defaultValue = "") String sortBy,
			@RequestParam(value = "searchby", defaultValue = "") String searchBy, 
			@PathVariable Long userId) {
		
		List<JobApplicationSubmitionDto> jobApplicationSubmitionDtoList = jobApplicationSubmitionService.getAllJobApplicationSubmitionInfoByApplicantId(page, limit, userId);
		List<JobApplicationSubmitionResponseModel> jobApplicationSubmitionResponseModelList = new ArrayList<>();
		for(JobApplicationSubmitionDto jobApplicationSubmitionDto: jobApplicationSubmitionDtoList) {
			jobApplicationSubmitionResponseModelList.add(jobApplicationSubmitionMapper.mapFromJobApplicationSubmitionDtoToJobApplicationSubmitionResponseModel(jobApplicationSubmitionDto));
		}
		return jobApplicationSubmitionResponseModelList;
		
	}
	
	@GetMapping(value = "profiles/{userId}/applied-jobs/{jobId}")
	public JobApplicationSubmitionResponseModel getGetUserAppliedJobInfoByJobId(
			@PathVariable Long userId,
			@PathVariable Long jobId) {
		
		JobApplicationSubmitionDto jobApplicationSubmitionDto =  jobApplicationSubmitionService.getAppliedJobInfoByApplicantIdAndJobId(userId, jobId);
		
		return jobApplicationSubmitionMapper.mapFromJobApplicationSubmitionDtoToJobApplicationSubmitionResponseModel(jobApplicationSubmitionDto);
	}
	
	@PostMapping(value = "profiles/{userId}/applied-jobs")
	public JobApplicationSubmitionResponseModel applyToSpecificJob(
			@RequestBody JobApplicationSubmitionRequestModel jobApplicationSubmitionRequestModel,
			@PathVariable long userId) {
		
		JobApplicationSubmitionDto jobApplicationSubmitionDto = jobApplicationSubmitionMapper.mapFromJobApplicationSubmitionRequestModelToJobApplicationSubmitionDto(jobApplicationSubmitionRequestModel);
		jobApplicationSubmitionDto = jobApplicationSubmitionService.applyJobByApplicant(jobApplicationSubmitionDto, userId);
		return jobApplicationSubmitionMapper.mapFromJobApplicationSubmitionDtoToJobApplicationSubmitionResponseModel(jobApplicationSubmitionDto);
	}

}
