package com.linkstaff.jobserviceapi.ws.ui.model.response;

public enum OperationStatus {
	ERROR, SUCCESS
}
