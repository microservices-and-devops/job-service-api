package com.linkstaff.jobserviceapi.ws.ui.model.request;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobRequestModel {

	private String name;
	private String description;
	private Date joiningDate;
}
