package com.linkstaff.jobserviceapi.ws.ui.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobApplicationSubmitionResponseModel {
	
	private Long id;
	private Long userId;
	private JobResponseModel job;

}
