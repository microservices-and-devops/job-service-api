package com.linkstaff.jobserviceapi.ws.ui.model.response;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobResponseModel {

	private Long id;
	private String name;
	private String description;
	private Date joiningDate;
}
