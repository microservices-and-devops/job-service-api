package com.linkstaff.jobserviceapi.ws.ui.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.linkstaff.jobserviceapi.ws.service.JobService;
import com.linkstaff.jobserviceapi.ws.shared.dto.JobDto;
import com.linkstaff.jobserviceapi.ws.ui.model.request.JobRequestModel;
import com.linkstaff.jobserviceapi.ws.ui.model.response.JobResponseModel;
import com.linkstaff.jobserviceapi.ws.ui.model.response.OperationName;
import com.linkstaff.jobserviceapi.ws.ui.model.response.OperationStatus;
import com.linkstaff.jobserviceapi.ws.ui.model.response.OperationStatusModel;
import com.linkstaff.jobserviceapi.ws.utils.mapper.JobMapper;



@RestController
@RequestMapping(path  = "/")
public class JobController {
	
	@Autowired
	JobService jobService;

	@Autowired
	JobMapper jobMapper;

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<JobResponseModel> getAllJob(
			@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "limit", defaultValue = "25") int limit) {
		List<JobResponseModel> jobResponseModelList = new ArrayList<>();

		List<JobDto> jobDtoList = jobService.getAllJobs(page, limit);
		
		for (JobDto jobDto : jobDtoList) {
			jobResponseModelList.add(jobMapper.mapFromJobDtoToJobResponseModel(jobDto));
		}

		return jobResponseModelList;
	}

	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public JobResponseModel getJobById(@PathVariable long id) {
		JobDto jobDto = jobService.getJobById(id);
		return jobMapper.mapFromJobDtoToJobResponseModel(jobDto);
	}

	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public JobResponseModel createJob(
			@RequestBody JobRequestModel jobRequestModel) {
		
		JobDto jobDto = jobMapper.mapFromJobRequestModelToJobDto(jobRequestModel);
		jobDto = jobService.createJob(jobDto);
		return jobMapper.mapFromJobDtoToJobResponseModel(jobDto);
	}

	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public JobResponseModel updateJobById( @PathVariable long id,
			@RequestBody JobRequestModel jobRequestModel) {
		
		JobDto jobDto = jobMapper.mapFromJobRequestModelToJobDto(jobRequestModel);

		jobDto = jobService.updateJob(jobDto, id);
		return jobMapper.mapFromJobDtoToJobResponseModel(jobDto);

	}

	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel deleteJobById( @PathVariable long id) {
		OperationStatusModel operationStatusModel = new OperationStatusModel();
		operationStatusModel.setOperationName(OperationName.DELETE.name());
		
			if (jobService.deleteJobById(id)) {
				operationStatusModel.setOperationResult(OperationStatus.SUCCESS.name());
			} else {
				operationStatusModel.setOperationResult(OperationStatus.ERROR.name());
			}


		return operationStatusModel;
	}

	@DeleteMapping(path = "hardDelete/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel hardDeleteJobById( @PathVariable long id) {
		OperationStatusModel operationStatusModel = new OperationStatusModel();
		operationStatusModel.setOperationName(OperationName.DELETE.name());
		jobService.hardDeleteJobById(id);
		operationStatusModel.setOperationResult(OperationStatus.SUCCESS.name());
		return operationStatusModel;
	}

}
