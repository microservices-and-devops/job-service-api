package com.linkstaff.jobserviceapi.ws.ui.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobApplicationSubmitionRequestModel {
	
	private Long jobId;

}
