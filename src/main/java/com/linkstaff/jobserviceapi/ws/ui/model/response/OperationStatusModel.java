package com.linkstaff.jobserviceapi.ws.ui.model.response;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class OperationStatusModel {

	private String operationName;
	private String operationResult;
}
