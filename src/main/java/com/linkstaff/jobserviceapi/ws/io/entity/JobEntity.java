package com.linkstaff.jobserviceapi.ws.io.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "job")
@Getter
@Setter
public class JobEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String description;
	private Date joiningDate;
	@Column(nullable = false)
	private boolean isDeleted = false;
	
	@OneToMany(mappedBy = "job", cascade = CascadeType.ALL)
	private List<JobApplicationSubmitionEntity> jobApplications = new ArrayList<JobApplicationSubmitionEntity>();
	

}
