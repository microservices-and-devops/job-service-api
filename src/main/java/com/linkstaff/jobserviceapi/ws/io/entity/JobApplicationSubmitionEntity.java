package com.linkstaff.jobserviceapi.ws.io.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;


@Entity(name = "JobApplicationSubmition")
@Getter
@Setter
public class JobApplicationSubmitionEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Long userId;
	@Column(nullable = false)
	private boolean isDeleted = false;
	
	@ManyToOne
	@JoinColumn(name = "jobId")
	private JobEntity job;
}
