package com.linkstaff.jobserviceapi.ws.utils.dataseeder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;
import com.linkstaff.jobserviceapi.ws.io.entity.JobApplicationSubmitionEntity;
import com.linkstaff.jobserviceapi.ws.io.entity.JobEntity;
import com.linkstaff.jobserviceapi.ws.repository.JobRepositoy;

@Component
public class DataSeeder implements CommandLineRunner {
	
	@Autowired
	JobRepositoy jobRepositoy;

	@Override
	public void run(String... args) throws Exception {

		List<JobApplicationSubmitionEntity> jobApplicants;

		Faker faker = new Faker(new Random(50));
		int MAX_ITEM = 200;
		int MAX_NESTED_ITEM = 50;

		for (int i = 0; i < MAX_ITEM; i++) {
			JobEntity jobEntity = new JobEntity();

			jobEntity.setDescription(faker.beer().name());
			jobEntity.setJoiningDate(DateUtils.addDays(new Date(), i + 20));
			jobEntity.setName(faker.lorem().sentence());

			jobApplicants = new ArrayList<>();

			for (int j = 0; j < MAX_NESTED_ITEM; j++) {

				// Setting value for JobApplicationSubmitionEntity
				JobApplicationSubmitionEntity jobApplicationSubmitionEntity = new JobApplicationSubmitionEntity();
				jobApplicationSubmitionEntity.setUserId(Long.valueOf(i + j));
				jobApplicationSubmitionEntity.setJob(jobEntity);
				jobApplicants.add(jobApplicationSubmitionEntity);

			}

			jobEntity.setJobApplications(jobApplicants);
			jobRepositoy.save(jobEntity);

		}

	}

}
