package com.linkstaff.jobserviceapi.ws.utils.mapper;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import com.linkstaff.jobserviceapi.ws.io.entity.JobEntity;
import com.linkstaff.jobserviceapi.ws.shared.dto.JobDto;
import com.linkstaff.jobserviceapi.ws.ui.model.request.JobRequestModel;
import com.linkstaff.jobserviceapi.ws.ui.model.response.JobResponseModel;

@Component
public class JobMapper {
	
	private ModelMapper modelMapper;
	TypeMap<JobDto, JobEntity> jobTypeMap;

	public JobMapper() {
		this.modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		this.jobTypeMap = modelMapper.createTypeMap(JobDto.class, JobEntity.class);
	}

	public JobDto mapFromJobRequestModelToJobDto(JobRequestModel jobRequestModel) {
		return modelMapper.map(jobRequestModel, JobDto.class);
	}
	
	public JobDto mapFromJobResponseModelToJobDto(JobResponseModel jobResponseModel) {
		return modelMapper.map(jobResponseModel, JobDto.class);
	}

	public JobDto mapFromJobEntityToJobDto(JobEntity jobEntity) {
		return modelMapper.map(jobEntity, JobDto.class);
	}

	public JobEntity mapFromJobDtoToJobEntity(JobDto jobDto, JobEntity jobEntity) {
		
		jobTypeMap.addMappings(mapper -> {
			mapper.skip(JobEntity::setId);
		});
		jobTypeMap.map(jobDto, jobEntity);
		return jobEntity;

	}
	public JobEntity mapAllFromJobDtoToJobEntity(JobDto jobDto) {
		return modelMapper.map(jobDto, JobEntity.class);
	}

	public JobResponseModel mapFromJobDtoToJobResponseModel(JobDto jobDto) {
		return modelMapper.map(jobDto, JobResponseModel.class);
	}

}
