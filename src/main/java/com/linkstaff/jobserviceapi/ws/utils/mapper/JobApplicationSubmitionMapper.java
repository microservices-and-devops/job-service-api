package com.linkstaff.jobserviceapi.ws.utils.mapper;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import com.linkstaff.jobserviceapi.ws.io.entity.JobApplicationSubmitionEntity;
import com.linkstaff.jobserviceapi.ws.shared.dto.JobApplicationSubmitionDto;
import com.linkstaff.jobserviceapi.ws.ui.model.request.JobApplicationSubmitionRequestModel;
import com.linkstaff.jobserviceapi.ws.ui.model.response.JobApplicationSubmitionResponseModel;


@Component
public class JobApplicationSubmitionMapper {
	
	private ModelMapper modelMapper;
	TypeMap<JobApplicationSubmitionDto, JobApplicationSubmitionEntity> jobApplicationSubmitionTypeMap;

	public JobApplicationSubmitionMapper() {
		this.modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		this.jobApplicationSubmitionTypeMap = modelMapper.createTypeMap(JobApplicationSubmitionDto.class, JobApplicationSubmitionEntity.class);
	}

	public JobApplicationSubmitionDto mapFromJobApplicationSubmitionRequestModelToJobApplicationSubmitionDto(JobApplicationSubmitionRequestModel jobApplicationSubmitionRequestModel) {
		return modelMapper.map(jobApplicationSubmitionRequestModel, JobApplicationSubmitionDto.class);
	}
	
	public JobApplicationSubmitionDto mapFromJobApplicationSubmitionResponseModelToJobApplicationSubmitionDto(JobApplicationSubmitionResponseModel jobApplicationSubmitionResponseModel) {
		return modelMapper.map(jobApplicationSubmitionResponseModel, JobApplicationSubmitionDto.class);
	}

	public JobApplicationSubmitionDto mapFromJobApplicationSubmitionEntityToJobApplicationSubmitionDto(JobApplicationSubmitionEntity jobApplicationSubmitionEntity) {
		return modelMapper.map(jobApplicationSubmitionEntity, JobApplicationSubmitionDto.class);
	}

	public JobApplicationSubmitionEntity mapFromJobApplicationSubmitionDtoToJobApplicationSubmition(JobApplicationSubmitionDto jobApplicationSubmitionDto, JobApplicationSubmitionEntity jobApplicationSubmitionEntity) {
		
		jobApplicationSubmitionTypeMap.addMappings(mapper -> {
			mapper.skip(JobApplicationSubmitionEntity::setId);
		});
		jobApplicationSubmitionTypeMap.map(jobApplicationSubmitionDto, jobApplicationSubmitionEntity);
		return jobApplicationSubmitionEntity;

	}
	public JobApplicationSubmitionEntity mapAllFromJobApplicationSubmitionDtoToJobApplicationSubmitionEntity(JobApplicationSubmitionDto registeredAttendeeForEventDto) {
		return modelMapper.map(registeredAttendeeForEventDto, JobApplicationSubmitionEntity.class);
	}

	public JobApplicationSubmitionResponseModel mapFromJobApplicationSubmitionDtoToJobApplicationSubmitionResponseModel(JobApplicationSubmitionDto jobApplicationSubmitionDto) {
		return modelMapper.map(jobApplicationSubmitionDto, JobApplicationSubmitionResponseModel.class);
	}

}
