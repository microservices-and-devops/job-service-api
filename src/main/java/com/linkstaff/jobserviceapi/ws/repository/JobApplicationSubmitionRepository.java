package com.linkstaff.jobserviceapi.ws.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.linkstaff.jobserviceapi.ws.io.entity.JobApplicationSubmitionEntity;
import com.linkstaff.jobserviceapi.ws.io.entity.JobEntity;

@Repository
public interface JobApplicationSubmitionRepository extends PagingAndSortingRepository<JobApplicationSubmitionEntity, Long> {

	List<JobApplicationSubmitionEntity> findAll();
	JobApplicationSubmitionEntity findById(long id);
	List<JobApplicationSubmitionEntity> findByUserId(long applicantId);
	JobApplicationSubmitionEntity findByUserIdAndJob(long userId, JobEntity job);
	
}
