package com.linkstaff.jobserviceapi.ws.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.linkstaff.jobserviceapi.ws.io.entity.JobEntity;

@Repository
public interface JobRepositoy extends PagingAndSortingRepository<JobEntity, Long> {

	List<JobEntity> findAll();
	JobEntity findById(long id);
}
