package com.linkstaff.jobserviceapi.ws.shared.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobDto {
	
	private Long id;
	private String name;
	private String description;
	private Date joiningDate;

}
