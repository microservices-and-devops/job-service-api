package com.linkstaff.jobserviceapi.ws.shared.dto;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class JobApplicationSubmitionDto {
	
	private Long id;
	private Long userId;
	private Long jobId;
	private JobDto job;

}
