package com.linkstaff.jobserviceapi.ws.service;

import java.util.List;

import com.linkstaff.jobserviceapi.ws.shared.dto.JobApplicationSubmitionDto;
import com.linkstaff.jobserviceapi.ws.ui.model.response.JobApplicationSubmitionResponseModel;


public interface JobApplicationSubmitionService{
	
	public List<JobApplicationSubmitionDto> getAllJobApplicationSubmitionInfoByApplicantId(int page, int limit, long applicantId);
	public List<JobApplicationSubmitionDto> getAllJobApplicantByJobId(int page, int limit, long jobId);
	public JobApplicationSubmitionDto getAppliedJobInfoByApplicantIdAndJobId(long applicantId, long jobId);
	public JobApplicationSubmitionDto applyJobByApplicant(JobApplicationSubmitionDto jobApplicationSubmitionDto, long jobId);
	public JobApplicationSubmitionDto updateAppliedJobByApplicant(JobApplicationSubmitionDto jobApplicationSubmitionDto, long id);
	public boolean cancelAppiedJobbuApplicant(long id);
	public boolean hardDeleteSpecificJobApplication(long id);

}
