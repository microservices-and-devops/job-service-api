package com.linkstaff.jobserviceapi.ws.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linkstaff.jobserviceapi.ws.io.entity.JobEntity;
import com.linkstaff.jobserviceapi.ws.repository.JobRepositoy;
import com.linkstaff.jobserviceapi.ws.service.JobService;
import com.linkstaff.jobserviceapi.ws.shared.dto.JobDto;
import com.linkstaff.jobserviceapi.ws.utils.mapper.JobMapper;

@Service
public class JobServiceImpl implements JobService {
	@Autowired
	JobRepositoy jobRepositoy;
	
	@Autowired
	JobMapper jobMapper;

	@Override
	public List<JobDto> getAllJobs(int page, int limit) {
		List<JobDto> jobDtoList = new ArrayList<>();

		List<JobEntity> jobs = jobRepositoy.findAll();

		for (JobEntity jobEntity : jobs) {
			jobDtoList.add(jobMapper.mapFromJobEntityToJobDto(jobEntity));
		}
		return jobDtoList;
	}

	@Override
	public JobDto getJobById(long id) {
		
		JobEntity jobEntity = jobRepositoy.findById(id);
		if (jobEntity == null)
			throw new RuntimeException("Job Record: not found");
		return jobMapper.mapFromJobEntityToJobDto(jobEntity);
	}

	@Override
	public JobDto createJob(JobDto jobDto) {
		
		JobEntity jobEntity = jobMapper.mapAllFromJobDtoToJobEntity(jobDto);
		jobEntity = jobRepositoy.save(jobEntity);
		return jobMapper.mapFromJobEntityToJobDto(jobEntity);
	}

	@Override
	public JobDto updateJob(JobDto jobDto, long id) {
		
		JobEntity jobEntity = jobRepositoy.findById(id);
		if (jobEntity == null)
			throw new RuntimeException("Job Record: not found");
	
		jobEntity = jobMapper.mapFromJobDtoToJobEntity(jobDto, jobEntity);
		jobEntity = jobRepositoy.save(jobEntity);
		return jobMapper.mapFromJobEntityToJobDto(jobEntity);
	}

	@Override
	public boolean deleteJobById(long id) {
		
		
		JobEntity jobEntity = jobRepositoy.findById(id);
		if (jobEntity == null)
			throw new RuntimeException("Job Record: not found");
		
		jobEntity.setDeleted(true);
		
		if (jobRepositoy.save(jobEntity).isDeleted()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean hardDeleteJobById(long id) {
		
		JobEntity jobEntity = jobRepositoy.findById(id);
		if (jobEntity == null)
			throw new RuntimeException("Job Record: not found");

		jobRepositoy.delete(jobEntity);
		return true;
	}

}
