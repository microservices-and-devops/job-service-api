package com.linkstaff.jobserviceapi.ws.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linkstaff.jobserviceapi.ws.io.entity.JobApplicationSubmitionEntity;
import com.linkstaff.jobserviceapi.ws.io.entity.JobEntity;
import com.linkstaff.jobserviceapi.ws.repository.JobApplicationSubmitionRepository;
import com.linkstaff.jobserviceapi.ws.repository.JobRepositoy;
import com.linkstaff.jobserviceapi.ws.service.JobApplicationSubmitionService;
import com.linkstaff.jobserviceapi.ws.shared.dto.JobApplicationSubmitionDto;
import com.linkstaff.jobserviceapi.ws.utils.mapper.JobApplicationSubmitionMapper;

@Service
public class JobApplicationSubmitionServiceImpl implements JobApplicationSubmitionService {

	@Autowired
	JobRepositoy jobRepositoy;

	@Autowired
	JobApplicationSubmitionRepository jobApplicationSubmitionRepository;

	@Autowired
	JobApplicationSubmitionMapper jobApplicationSubmitionMapper;

	@Override
	public List<JobApplicationSubmitionDto> getAllJobApplicationSubmitionInfoByApplicantId(int page, int limit,
			long applicantId) {

		List<JobApplicationSubmitionDto> jobApplicationSubmitionDtos = new ArrayList<JobApplicationSubmitionDto>();
		List<JobApplicationSubmitionEntity> jobApplicationSubmitionEntities = jobApplicationSubmitionRepository
				.findByUserId(applicantId);

		for (JobApplicationSubmitionEntity jobApplicationSubmitionEntity : jobApplicationSubmitionEntities) {
			jobApplicationSubmitionDtos.add(jobApplicationSubmitionMapper
					.mapFromJobApplicationSubmitionEntityToJobApplicationSubmitionDto(jobApplicationSubmitionEntity));
		}

		return jobApplicationSubmitionDtos;
	}

	@Override
	public List<JobApplicationSubmitionDto> getAllJobApplicantByJobId(int page, int limit, long jobId) {

		List<JobApplicationSubmitionDto> appliedJobDtos = new ArrayList<JobApplicationSubmitionDto>();
		JobEntity jobEntity = jobRepositoy.findById(jobId);

		for (JobApplicationSubmitionEntity jobApplicationSubmitionEntity : jobEntity.getJobApplications()) {
			appliedJobDtos.add(jobApplicationSubmitionMapper.mapFromJobApplicationSubmitionEntityToJobApplicationSubmitionDto(jobApplicationSubmitionEntity));
		}
		return appliedJobDtos;
	}

	@Override
	public JobApplicationSubmitionDto getAppliedJobInfoByApplicantIdAndJobId(long applicantId, long jobId) {
		
		JobEntity jobEntity = jobRepositoy.findById(jobId);
		JobApplicationSubmitionEntity jobApplicationSubmitionEntity = jobApplicationSubmitionRepository.findByUserIdAndJob(applicantId, jobEntity);
		return jobApplicationSubmitionMapper.mapFromJobApplicationSubmitionEntityToJobApplicationSubmitionDto(jobApplicationSubmitionEntity);
	}

	@Override
	public JobApplicationSubmitionDto applyJobByApplicant(JobApplicationSubmitionDto jobApplicationSubmitionDto, long userId) {
		
		
		JobEntity jobEntity= jobRepositoy.findById(jobApplicationSubmitionDto.getJobId()).get();
		if(jobEntity == null)
			throw new RuntimeException("Job Record: not found");
		JobApplicationSubmitionEntity jobApplicationSubmitionEntity = new JobApplicationSubmitionEntity();
		jobApplicationSubmitionEntity.setUserId(userId);

		if(jobEntity.getJobApplications()==null) {
			List<JobApplicationSubmitionEntity> jobApplicationSubmitionEntities = new ArrayList<>();
			jobApplicationSubmitionEntities.add(jobApplicationSubmitionEntity);
			jobEntity.setJobApplications(jobApplicationSubmitionEntities);
		}else {
			jobEntity.getJobApplications().add(jobApplicationSubmitionEntity);
		}
		
		jobApplicationSubmitionEntity.setJob(jobEntity);
		jobApplicationSubmitionEntity = jobApplicationSubmitionRepository.save(jobApplicationSubmitionEntity);
		return jobApplicationSubmitionMapper.mapFromJobApplicationSubmitionEntityToJobApplicationSubmitionDto(jobApplicationSubmitionEntity);
	}

	@Override
	public JobApplicationSubmitionDto updateAppliedJobByApplicant(JobApplicationSubmitionDto jobApplicationSubmitionDto,
			long id) {
		JobApplicationSubmitionEntity jobApplicationSubmitionEntity = jobApplicationSubmitionRepository.findById(id);
		if (jobApplicationSubmitionEntity == null)
			throw new RuntimeException("JobApplicationSubmitionEntity Record: not found");
		
		jobApplicationSubmitionEntity = jobApplicationSubmitionMapper.mapFromJobApplicationSubmitionDtoToJobApplicationSubmition(jobApplicationSubmitionDto, jobApplicationSubmitionEntity);
		jobApplicationSubmitionEntity = jobApplicationSubmitionRepository.save(jobApplicationSubmitionEntity);

		return jobApplicationSubmitionMapper.mapFromJobApplicationSubmitionEntityToJobApplicationSubmitionDto(jobApplicationSubmitionEntity);
	}

	@Override
	public boolean cancelAppiedJobbuApplicant(long id) {
		JobApplicationSubmitionEntity jobApplicationSubmitionEntity = jobApplicationSubmitionRepository.findById(id);
		if (jobApplicationSubmitionEntity == null)
			throw new RuntimeException("JobApplicationSubmitionEntity Record: not found");
		
		jobApplicationSubmitionEntity.setDeleted(true);

		if (jobApplicationSubmitionRepository.save(jobApplicationSubmitionEntity).isDeleted()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean hardDeleteSpecificJobApplication(long id) {
		JobApplicationSubmitionEntity jobApplicationSubmitionEntity = jobApplicationSubmitionRepository.findById(id);
		if (jobApplicationSubmitionEntity == null)
			throw new RuntimeException("JobApplicationSubmitionEntity Record: not found");
		
		jobApplicationSubmitionRepository.delete(jobApplicationSubmitionEntity);
		return true;
	}

}
