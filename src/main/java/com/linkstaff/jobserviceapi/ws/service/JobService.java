package com.linkstaff.jobserviceapi.ws.service;

import java.util.List;

import com.linkstaff.jobserviceapi.ws.shared.dto.JobDto;


public interface JobService {
	
	public List<JobDto> getAllJobs(int page, int limit);
	public JobDto getJobById(long id);
	public JobDto createJob(JobDto jobDto);
	public JobDto updateJob(JobDto jobDto, long id);
	public boolean deleteJobById(long id);
	public boolean hardDeleteJobById(long id);

}
