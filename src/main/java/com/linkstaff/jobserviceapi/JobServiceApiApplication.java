package com.linkstaff.jobserviceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobServiceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobServiceApiApplication.class, args);
	}

}
